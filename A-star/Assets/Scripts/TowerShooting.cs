﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShooting : MonoBehaviour {

   public  GameObject fireAt;
    public GameObject bullet;
    public Transform fireFrom;
	// Use this for initialization
	void Start () {
        fireAt = GameObject.FindGameObjectWithTag("Monster");
        bullet = GameObject.FindGameObjectWithTag("Bullet");
    }
	
	// Update is called once per frame
	void Update () {
        Move();
        Instantiate(bullet, fireFrom);
    }
    public void Move()
    {
        //if (Vector3.Distance(transform.position, fireAt.transform.position) < 1f)
        //{
        //    index++;
        //}
        transform.position = Vector3.MoveTowards(transform.position, fireAt.transform.position, 5f * Time.deltaTime);
    }
}
