﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Grid gridReference;
    public Transform StartPosition;
    public Transform TargetPosition;

    private void Awake()
    {
        gridReference = GetComponent<Grid>();
    }

    private void Update()
    {
        FindPath(StartPosition.position, TargetPosition.position);
    }

    void FindPath(Vector3 startPosition, Vector3 targetPosition)
    {
        Node startNode = gridReference.NodeFromWorldPoint(startPosition);
        Node targetNode = gridReference.NodeFromWorldPoint(targetPosition);

        List<Node> OpenList = new List<Node>(); //list of nodes for the open list
        HashSet<Node> ClosedList = new HashSet<Node>();

        OpenList.Add(startNode); //add the starting node to the open list to begin the program

        while (OpenList.Count > 0)
        {
            Node currentNode = OpenList[0]; //Create a node and set it to the first item in the open list

            for (int i = 1; i < OpenList.Count; i++)
            {
                // if the f cost of that object is less than or equal to the f cost of the curret node
                if (OpenList[i].F_Cost < currentNode.F_Cost || OpenList[i].F_Cost == currentNode.F_Cost && OpenList[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = OpenList[i];
                }
            }

            OpenList.Remove(currentNode);
            ClosedList.Add(currentNode);

            // if the current node is the same as target node
            if (currentNode == targetNode) 
            {
                GetFinalPath(startNode, targetNode);
            }


            // Loop through each neighbor of the current node
            foreach (var neighborNode in gridReference.GetNeighboringNodes(currentNode))
            {
                if (!neighborNode.IsWall || ClosedList.Contains(neighborNode)) // If the neighbor node is a wall or if it has been already checked
                {
                    continue; // skip
                }

                int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neighborNode); // Get the F Cost of that neighbor

                if (moveCost < neighborNode.G_Cost || !OpenList.Contains(neighborNode)) // If the f cost is greater than the g cost or it is not in the open list
                {
                    neighborNode.G_Cost = moveCost; // set the g cost to the f cost
                    neighborNode.H_Cost = GetManhattanDistance(neighborNode, targetNode); // set the h cost
                    neighborNode.ParentNode = currentNode; // set the parent of the node for retracing steps

                    if (!OpenList.Contains(neighborNode))
                    {
                        OpenList.Add(neighborNode);
                    }
                }
            }
        }
    }

    void GetFinalPath(Node startingNode, Node endNode)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startingNode)
        {
            finalPath.Add(currentNode); // add that node to the final path
            currentNode = currentNode.ParentNode; // move on to its parent node
        }

        finalPath.Reverse();
        gridReference.FinalPath = finalPath;
    }

    int GetManhattanDistance(Node a, Node b)
    {
        int x = Mathf.Abs(a.GridX - b.GridX); // x1 - x2
        int y = Mathf.Abs(a.GridY - b.GridY); // y1 - y2;

        return x + y;
    }
}
