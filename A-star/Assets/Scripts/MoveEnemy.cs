﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour {

    private Grid grid;
    private int index = 0;
	// Use this for initialization
	void Start () {
        grid = GameObject.Find("GameManager").GetComponent<Grid>();
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    public void Move()
    {
        if (Vector3.Distance(transform.position, grid.FinalPath[index].Position) < 1f)
        {
            index++;
        }
        transform.position = Vector3.MoveTowards(transform.position, grid.FinalPath[index].Position, 5f * Time.deltaTime);
    }
}
