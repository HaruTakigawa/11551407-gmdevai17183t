﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectionComponent : MonoBehaviour {

    private bool isSelecting = false;
    private Vector3 mousePosition;
    private List<Transform> selectObject = new List<Transform>();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.doUnitSelection();
	}
    private void doUnitSelection()
    {
        // if we press the left mouse button begin selection and remember the location of the mouse

        if (Input.GetMouseButtonDown(0))
        {
            isSelecting = true;
            mousePosition = Input.mousePosition;
            foreach (var selectableObject in FindObjectsOfType<SelectableUnitComponent>())
            {
                //Disable and remove selection
                if (selectableObject.IsSelected())
                {
                    Renderer rend = selectableObject.GetComponent<Renderer>();
                    rend.material.color = Color.white;
                    selectableObject.SetSelection(false);
                }
            }
        }

        // if we let go of the left mouse, end selection
        if (Input.GetMouseButtonUp(0))
        {
            foreach (var selectableObject in FindObjectsOfType<SelectableUnitComponent>())
            {
                if (this.isInBound(selectableObject.gameObject))
                {
                    selectObject.Add(selectableObject.transform);
                }
            }
            isSelecting = false;
        }

        //highlight all objects within selection box
        if (isSelecting)
        {
            foreach(var selectableObject in FindObjectsOfType<SelectableUnitComponent>())
            {
                if (this.isInBound(selectableObject.gameObject))
                {
                    selectableObject.SetSelection(true);
                    Renderer rend = selectableObject.GetComponent<Renderer>();
                    rend.material.color = Color.green;
                }
                else
                {
                    if(selectableObject.IsSelected())
                    {
                        Renderer rend = selectableObject.GetComponent<Renderer>();
                        rend.material.color = Color.white;
                        selectableObject.SetSelection(false);
                    }
                }
            }
        }
    }

    private bool isInBound(GameObject gameObject)
    {
        if (!isSelecting) return false;
        var camera = Camera.main;
        var viewportBound = Utils.GetViewportBounds(camera, mousePosition, Input.mousePosition);
        return viewportBound.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            //create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePosition, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
        
    }

    public List<Transform> getSelectedObjects()
    {
        return selectObject;
    }

    public void ClearSelection()
    {
        this.selectObject.Clear();
    }
}
