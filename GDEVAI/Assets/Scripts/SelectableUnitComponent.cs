﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableUnitComponent : MonoBehaviour {

    [SerializeField]
    private bool selection = false;

    public void SetSelection(bool value)
    {
        this.selection = value;
    }

    public bool IsSelected()
    {
        return selection;
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
