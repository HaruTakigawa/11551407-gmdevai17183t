﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortestPath : MonoBehaviour
{
    private GameObject[] nodes;

    public List<Transform> FindShortestPath(Transform start, Transform end)
    {
        nodes = GameObject.FindGameObjectsWithTag("Node");

        List<Transform> result = new List<Transform>();
        Transform node = Dijkstra(start, end);

        //while there is still previous node, continue

        while(node!= null)
        {
            result.Add(node);
            Node currentNode = node.GetComponent<Node>();
            node = currentNode.GetParentNode();
        }
        //reverse the list so that it will be from start to end
        result.Reverse();
        return result;
    }

    private Transform Dijkstra(Transform start, Transform end)
    {
        double startTime = Time.realtimeSinceStartup;

        // nodes that are not visited
        List<Transform> unexplored = new List<Transform>();

        foreach (GameObject obj in nodes)
        {
            Node n = obj.GetComponent<Node>();

            if (n.GetisWalkable())
            {
                n.ResetNode();
                unexplored.Add(obj.transform);
            }
        }

            //Set the starting node weight to 0
            Node startNode = start.GetComponent<Node>();
            startNode.SetWeight(0);

            while (unexplored.Count > 0)
            {
                //sort the explored by weight in ascending order
                unexplored.Sort((x, y) => x.GetComponent<Node>().GetWeight().CompareTo(y.GetComponent<Node>().GetWeight()));

                // Get the lowest weight from unexplored
                Transform current = unexplored[0];

                //remove the current node,since we visited it now
                unexplored.Remove(current);

                Node currentNode = current.GetComponent<Node>();
                List<Transform> neighbors = currentNode.getNeighborNode();

                foreach (Transform neighbor in neighbors)
                {
                    Node node = neighbor.GetComponent<Node>();

                    //avoid not walkable and visited nodes
                    if (unexplored.Contains(neighbor) && node.GetisWalkable())
                    {

                        //get the distance of the object
                        float distance = Vector3.Distance(neighbor.position, current.position);
                        distance += currentNode.GetWeight();


                        // if the added distance is less than the current weight
                        if (distance < node.GetWeight())
                        {
                            //We update the new distance as weight and update the new path
                            node.SetWeight(distance);
                            node.SetParentNode(current);
                        }
                    }
                }
            }
            double endTime = (Time.realtimeSinceStartup - startTime);
            print("Compute time: " + endTime);

            return end;
         
    }
}
