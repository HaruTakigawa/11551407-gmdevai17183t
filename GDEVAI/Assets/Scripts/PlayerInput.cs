﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    private Transform node;
    private Transform startNode;
    private Transform endNode;
    private List<Transform> blockPath = new List<Transform>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ListenForMousInpute();
	}

    private void ListenForMousInpute()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Update colors for every mouse click
            this.ColorBlockPath();
            this.updateNodeColor();

            //get the raycast from the mouse position from screen
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit)&& hit.transform.tag == "Node")
            {
                //unmark previous
                Renderer rend;
                if(node != null)
                {
                    rend = node.GetComponent<Renderer>();
                    rend.material.color = Color.white;
                }

                //we now update the selected node
                node = hit.transform;

                //mark it
                rend = node.GetComponent<Renderer>();
                rend.material.color = Color.green;
            }
        }
        
    }
    private void ColorBlockPath()
    {
        foreach (var block in blockPath)
        {
            Renderer renderer = block.GetComponent<Renderer>();
            renderer.material.color = Color.black;
        }
    }
    private void updateNodeColor()
    {
        if(startNode != null)
        {
            Renderer renderer = startNode.GetComponent<Renderer>();
            renderer.material.color = Color.blue;
        }
        if(endNode != null)
        {
            Renderer renderer = endNode.GetComponent<Renderer>();
            renderer.material.color = Color.cyan;
        }
    }

    public void BtnStartNode()
    {
        if(node != null)
        {
            Node n = node.GetComponent<Node>();

            //We make sure that only a walkable node is selected as start
            if(n.GetisWalkable())
            {
                //if this is a new start node, we will just set it to blue
                if (startNode == null)
                {
                    Renderer rend = node.GetComponent<Renderer>();
                    rend.material.color = Color.blue;
                }
            }
            else
            {
                //Reverse the color of the previous start node
                Renderer rend = startNode.GetComponent<Renderer>();
                rend.material.color = Color.white;

                //Set the new node as blue
                rend = node.GetComponent<Renderer>();
                rend.material.color = Color.blue;
            }
            startNode = node;
            node = null;
        }
    }
    public void BtnEndNode()
    {
        if (node != null)
        {
            Node n = node.GetComponent<Node>();

            //We make sure that only a walkable node is selected as end
            if (n.GetisWalkable())
            {
                //if this is a new end node, we will just set it to cyan
                if (endNode == null)
                {
                    Renderer rend = node.GetComponent<Renderer>();
                    rend.material.color = Color.cyan;
                }
            }
            else
            {
                //Reverse the color of the previous end node
                Renderer rend = endNode.GetComponent<Renderer>();
                rend.material.color = Color.white;

                //Set the new node as cyan
                rend = node.GetComponent<Renderer>();
                rend.material.color = Color.cyan;
            }
            endNode = node;
            node = null;
        }
    }
    public void BtnFindPath()
    {
        //only find if there are start and end nodes
        if(startNode != null && endNode != null)
        {
            //Execute shortest path
            ShortestPath finder = gameObject.GetComponent<ShortestPath>();
            List<Transform> paths = finder.FindShortestPath(startNode, endNode);

            // Color Node red
            foreach(var path in paths)
            {
                Renderer rend = path.GetComponent<Renderer>();
                rend.material.color = Color.red;
            }
        }
    }

    public void BtnBlockPath()
    {
        if(node!= null)
        {
            //Render the selected node to black
            Renderer rend = node.GetComponent<Renderer>();
            rend.material.color = Color.black;

            //set the selected node to not walkable
            Node n = node.GetComponent<Node>();
            n.SetWalkable(false);

            //Add the node to the black path list
            blockPath.Add(node);

            // if the block path is the start node, we remove the start node
            if(node == startNode)
            {
                startNode = null;
            }

            // if the block path is the end node, we remove the end node
            if(node == endNode)
            {
                endNode = null;
            }

            node = null;
        }
        //for selection grid system
        UnitSelectionComponent selection = gameObject.GetComponent<UnitSelectionComponent>();
        List<Transform> selected = selection.getSelectedObjects();

        foreach(Transform t in selected)
        {
            //render the selected node to black
            Renderer rend = t.GetComponent<Renderer>();
            rend.material.color = Color.black;

            //set selected node to not walkable
            Node n = t.GetComponent<Node>();
            n.SetWalkable(false);

            //add the node tothe block path list
            blockPath.Add(t);

            //if the block path is the start node, we remove the start node
            if (node == startNode)
            {
                startNode = null;
            }

            // if the block path is the end node, we remove the end node
            if (node == endNode)
            {
                endNode = null;
            }

        }
        selection.ClearSelection();
    }
}




