﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    [SerializeField]
    private float weight = int.MaxValue;
    [SerializeField]
    private Transform parentNode = null;

    [SerializeField]
    private List<Transform> neighborNode;

    [SerializeField]
    private bool isWalkable = true;

	// Use this for initialization
	void Start () {
        this.ResetNode();
	}

    public void ResetNode()
    {
        weight = int.MaxValue;
        parentNode = null;
    }

    //setter

    public void SetParentNode(Transform node)
    {
        this.parentNode = node;
    }
    public void SetWeight(float value)
    {
        this.weight = value;
    }

    public void SetWalkable(bool value)
    {
        this.isWalkable = value;
    }

    public void AddNeighborNode(Transform node)
    {
        this.neighborNode.Add(node);
    }
    public List<Transform> getNeighborNode()
    {
        return this.neighborNode;
    }
    
    public float GetWeight()
    {
        return this.weight;
    }

    public Transform GetParentNode()
    {
        return this.parentNode;
    }

    public bool GetisWalkable()
    {
        return this.isWalkable;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
