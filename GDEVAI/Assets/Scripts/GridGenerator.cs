﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    public int row = 5;
    public int column = 5;
    public float padding = 1.1f;
    public Transform nodePrefab;

    public List<Transform> grid = new List<Transform>();
	// Use this for initialization
	void Start () {
        GenerateGrid();
        GenerateNeighbor();
	}
	
    private void GenerateGrid()
    {
        int counter = 0;
        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < column; j++)
            {
                Transform node = Instantiate(nodePrefab, new Vector3((j * padding) + gameObject.transform.position.x, gameObject.transform.position.y, (i * padding) + gameObject.transform.position.z), Quaternion.identity);

                node.name = "Node(" + counter + ")";
                grid.Add(node);
                counter++;
            }
        }
    }

    private void GenerateNeighbor()
    {
        for(int i = 0; i < grid.Count;i++)
        {
            Node currentNode = grid[i].GetComponent<Node>();
            int index = i + 1;

            //for those on the left with no left neighbours
            if(index % column ==1)
            {
                //We want the node at the top as long as there is a node.
                if(i+column < column* row)
                {
                    currentNode.AddNeighborNode(grid[i + column]); //North Node
                }
                if (i - column >= 0)
                {
                    currentNode.AddNeighborNode(grid[i - column]); //South Node
                }
                currentNode.AddNeighborNode(grid[i + 1]); //East Node
            }
            // for those on the right with no right neighbours
            else if (index % column == 0)
            {
                // we want the node at the top as long as there is a node
                if (i + column < column * row)
                {
                    currentNode.AddNeighborNode(grid[i + column]); //North Node
                }
                if (i - column >= 0)
                {
                    currentNode.AddNeighborNode(grid[i - column]); //South Node
                }
                currentNode.AddNeighborNode(grid[i - 1]); //West Node
            } 
            else
            {
                // We want the node at the top as long as there is a node
                if (i + column < column * row)
                {
                    currentNode.AddNeighborNode(grid[i + column]); //North Node
                }
                if (i - column >= 0)
                {
                    currentNode.AddNeighborNode(grid[i - column]); //South Node
                }
                currentNode.AddNeighborNode(grid[i + 1]); //East Node
                currentNode.AddNeighborNode(grid[i - 1]); //West Node
            }

        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
