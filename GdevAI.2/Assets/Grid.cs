﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{

    public Transform StartPosition;
    public LayerMask WallMask; // this is the mask that the program will look for when trying to find obstruction e.g. walls and stuff
    public Vector2 GridSize;
    public float NodeRadius; // this stores how big each square on the graph will be
    public float DistanceBetweenNodes; // the distance that the square will spawn from each other

    Node[,] NodeArray; // The array of nodes that the A* algorithm uses
    public List<Node> FinalPath; // completed path

    float NodeDiameter;
    int gridSizeX, gridSizeY;// Size of the grid in Array units

    private void Start()
    {
        NodeDiameter = NodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(GridSize.x / NodeDiameter);
        gridSizeY = Mathf.RoundToInt(GridSize.y / NodeDiameter);
        CreateGrid();
    }
    void CreateGrid()
    {
        NodeArray = new Node[gridSizeX, gridSizeY];
        Vector3 bottomLeft = transform.position - Vector3.right * GridSize.x / 2 - Vector3.forward * GridSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)// loop through the array of nodes
        {
            for (int y = 0; x < gridSizeY; y++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * NodeDiameter + NodeRadius) + Vector3.forward * (y * NodeDiameter + NodeRadius); // get the world coordinates of the bootm left of the graph

                bool isWallFlag = true;
                //if the node is not obstructed
                //QUick collision check against the current node and anything in the world at its position. if it is colliding with an object with a wallmask, the statement will return false
                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    isWallFlag = false; // Object is not a wall
                }
                NodeArray[x,y] =  new Node(isWallFlag, worldPoint, x, y);
            }
        }
    }
    /**
     * function that gets the neigboring nodes of the given node
     **/

    public List<Node> GetNeighboringNodes(Node node)
    {
        List<Node> neighborList = new List<Node>();
        int checkX;// variable to tcheck if the xpos is within the range of the node array to avoid out of range errors
        int checkY;// variable to tcheck if the ypos is within the range of the node array to avoid out of range errors

        //check the right side of the current node
        checkX = node.GridX + 1;
        checkY = node.GridY;
        if (checkX >= 0 && checkX < gridSizeX)// check if xpos is in range
        {
            if(checkY>=0 && checkY < gridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]); // Add the grid to he availabale neighbors list
            }
        }
        //Check the Left side of the current node.
        checkX = node.GridX - 1;
        checkY = node.GridY;
        if (checkX >= 0 && checkX < gridSizeX)//If the XPosition is in range of the array
        {
            if (checkY >= 0 && checkY < gridSizeY)//If the YPosition is in range of the array
            {
                neighborList.Add(NodeArray[checkX, checkY]);//Add the grid to the available neighbors list
            }
        }
        //Check the Top side of the current node.
        checkX = node.GridX;
        checkY = node.GridY + 1;
        if (checkX >= 0 && checkX < gridSizeX)//If the XPosition is in range of the array
        {
            if (checkY >= 0 && checkY < gridSizeY)//If the YPosition is in range of the array
            {
                neighborList.Add(NodeArray[checkX, checkY]);//Add the grid to the available neighbors list
            }
        }
        //check the bottom side of the current node
        checkX = node.GridX;
        checkY = node.GridY-1;

        if(checkX>= 0 &&checkX < gridSizeX)
        {
            if(checkY >= 0 && checkY < gridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }
        return neighborList;
    }

    //gets the closest node to the given world position

    public Node NodeFromWorldPoint(Vector3 worldPoint)
    {
        float xPos = ((worldPoint.x + GridSize.x / 2) / GridSize.x);
        float yPos = ((worldPoint.z + GridSize.y / 2) / GridSize.y);

        xPos = Mathf.Clamp01(xPos);
        yPos = Mathf.Clamp01(yPos);

        int x = Mathf.RoundToInt((gridSizeX - 1) * xPos);
        int y = Mathf.RoundToInt((gridSizeY - 1) * yPos);

        return NodeArray[x, y];
    }

    //Draw the wireframe
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridSize.x, 1, gridSizeY));

        if (NodeArray != null)
        {
            foreach (Node n in NodeArray)
            {
                if (n.IsWall)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.yellow;
                }

                if(FinalPath!= null)// if the final path is not empty
                {
                    if(FinalPath.Contains(n))// if the current node is the final path
                    {
                        Gizmos.color = Color.red;
                    }
                }
                Gizmos.DrawCube(n.Position, Vector3.one * (NodeDiameter - DistanceBetweenNodes));
            }
        }
    }
}
