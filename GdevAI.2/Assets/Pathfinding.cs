﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    Grid gridReferece;
    public Transform StartPosition;
    public Transform TargetPosition;

    private void Awake()
    {
        gridReferece = GetComponent<Grid>();
    }

    private void Update()
    {
        FindPath(StartPosition.position, TargetPosition.position);
        
    }

    void FindPath(Vector3 startPosition,Vector3 targetPosition)
    {
        Node startNode = gridReferece.NodeFromWorldPoint(startPosition);
        Node targetNode = gridReferece.NodeFromWorldPoint(targetPosition);

        List<Node> OpenList = new List<Node>(); // this is the list of nodes for the open list
        HashSet<Node> ClosedList = new HashSet<Node>();

        OpenList.Add(startNode); // add the starting nonde to the open list to begin the program

        while(OpenList.Count > 0)
        {
            Node currentNode = OpenList[0]; // create a node and set it to the first item in the list

            for(int i = 0; i < OpenList.Count; i++)
            {
                // if the f cost of that object is less than or equal to the f cost of the current node
                if (OpenList[i].F_Cost < currentNode.F_Cost || OpenList[i].F_Cost == currentNode.F_Cost && OpenList[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = OpenList[i];
                }
            }

            OpenList.Remove(currentNode);
            ClosedList.Add(currentNode);

            // if the current node is the same as target node
        if(currentNode == targetNode)
            {
                GetFinalPath(startNode, targetNode);
            }
        // Looop through each neighbor of the current node
        foreach(var neighborNode in gridReferece.GetNeighboringNodes(currentNode))
            {
                if(!neighborNode.IsWall || ClosedList.Contains(neighborNode))// if the neighbore node is a wall if it has been already checked
                {
                    continue;// skip
                }
                int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neighborNode);// get the f cost of that neighbor

                if(moveCost < neighborNode.G_Cost || OpenList.Contains(neighborNode)) // of the f cpst os greater tham the g cost or it is not in the open list
                {
                    neighborNode.G_Cost = moveCost; // set the g cost to the f cost
                    neighborNode.H_Cost = GetManhattanDistance(neighborNode, targetNode); // set the h cost
                    neighborNode.ParentNode = currentNode; // set the parent of the node for retracing steps

                    if (!OpenList.Contains(neighborNode))
                    {
                        OpenList.Add(neighborNode);
                    }
                }
            }
        }
    }
    void GetFinalPath(Node startingNode, Node endNode)
    {
        List<Node> FinalPath = new List<Node>();
        Node currentNode = endNode;

        while(currentNode != startingNode)
        {
            FinalPath.Add(currentNode); // add that node to the final path
            currentNode = currentNode.ParentNode; // move on to its parent node
        }
        FinalPath.Reverse();
        gridReferece.FinalPath = FinalPath;
    }
    int GetManhattanDistance(Node a, Node b)
    {
        int x = Mathf.Abs(a.GridX - b.GridX); // x1 - x2
        int y = Mathf.Abs(a.GridY - b.GridY); // y1 - y2

        return x + y;
    }
}
